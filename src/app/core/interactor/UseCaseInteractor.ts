export interface UseCaseInteractor<REQUEST, RESPONSE> {
    execute(req:REQUEST):Promise<RESPONSE>;
}