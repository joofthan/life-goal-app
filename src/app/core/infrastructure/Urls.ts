export class Urls{
    private static dev = process.env.NODE_ENV === 'development';
    public static base = Urls.dev ? "" : "/life-goal-app";
    public static home = Urls.base+"/"; 
    public static lockscreen = Urls.base+"/lockscreen"; 
    public static menue = Urls.base+"/menue"; 
    public static addgoal = Urls.base+"/addgoal"; 
    public static details = Urls.base+"/details"
}