import { lang } from "../../feature/goal/infrastructure/language";

export class Database{

    dbNameWithVersion:string

    constructor(dbName:string, version:number){
        this.dbNameWithVersion = dbName + "_v"+version;
        let dbVersion:number = Number(localStorage.getItem(dbName+"_version"));
        if(dbVersion === 0){
            localStorage.setItem(dbName+"_version", String(version));
            //EmptyDB is ok
            return;
        }
        if(dbVersion != version){
            alert(lang.message.new_database);
            localStorage.clear();
            localStorage.setItem(dbName+"_version", String(version));
        }
    }

    async write(key:string, value:any){
        localStorage.setItem(this.dbNameWithVersion+"_"+key, JSON.stringify(value));
        //console.log("writing Storage: "+this.dbName + "_"+key);
        //console.log(value);
    }
    async load(key:string, defaultValue:any):Promise<any>{
        let data:string = localStorage.getItem(this.dbNameWithVersion+"_"+key);
        if(data == null){
            return defaultValue;
        }
        //console.log("loading Storage: "+this.dbName + "_"+key);
        //console.log(JSON.parse(data));
        return JSON.parse(data);
    }
}