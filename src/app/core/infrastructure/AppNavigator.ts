import { Urls } from "./Urls";
import { writable } from 'svelte/store';

export class AppNavigator{

    page:string = Urls.home;
    //_pageStore = writable("");
    listeners = [];

    constructor(){
        //this._pageStore.subscribe((val)=>this.page = val);
    }

    public home(){
        this.navigate(Urls.home);
    }

    public isHome():boolean{
        return this.isPage(Urls.home);
    }

    public addgoal(){
        this.navigate(Urls.addgoal);
    }

    public isAddgoal():boolean{
        return this.isPage(Urls.addgoal);
    }

    public details(id:number){
        this.navigate(Urls.details + "?id="+id);
    }

    public isDetails():boolean{
        return this.isPage(Urls.details);
    }

    public lockscreen(){
        this.navigate(Urls.lockscreen);
    }

    public isLockscreen():boolean{
        return this.isPage(Urls.lockscreen);
    }

    public getParam(name:string):string{
        try{
            let paramPart = this.page.split("?")[1];
            let na = "&"+paramPart+"&";
            let valueStart = na.split("&"+name+"=")[1];
            let value = valueStart.split("&")[0];
            return value;
        }catch(e){
            throw new Error("Param "+name+" not found. On: "+this.page+"."+e);
        }
    }

    private navigate(url:string){
        console.log("navigate to: "+ url);
        this.page = url;
        this.listeners.forEach((l)=>l());
        //this._pageStore.set(url);
        //location.href = url;
    }
    private isPage(url:string):boolean{
        return (this.page.split("?")[0]) === url;
    }

    public onChange(fun: () => void){
        //this._pageStore.subscribe(fun);
        this.listeners.push(fun);
    }
}

export const navigator = new AppNavigator();