import type { BaseEntity } from "./BaseEntity";
import { Database } from "./Database";


export abstract class BaseRepository<T extends BaseEntity>{
    db = new Database('life-goal-app', 1);
    key: string;

    constructor(key:string){
        if(key == null){
            throw new Error('Key is null');
        }
        this.key = key;
    }

    async findAll():Promise<T[]>{
        return this.db.load(this.key, []);
    }

    async findById(id:number):Promise<T>{
        let entityList = await this.db.load(this.key, []);
        for(let i=0; i < entityList.length; i++){
            if(entityList[i].id == id){
                return entityList[i];
            }
        }
        throw new Error("Goal with id "+id+" not found.");
    }

    async save(entity:T){
        let entityList = await this.db.load(this.key, []);
        if(entity.id == null){
            entity.id = new Date().getTime();
            entityList.push(entity);
        }else{
            for(let i=0; i < entityList.length; i++){
                if(entityList[i].id == entity.id){
                    entityList[i] = entity;
                }
            }
        }

        this.db.write(this.key, entityList);
    }
    async delete(id:number){
        let entityList = await this.db.load(this.key, []);
        for(let i=0; i < entityList.length; i++){
            if(entityList[i].id == id){
                entityList.splice(i,1);
            }
        }
        this.db.write(this.key, entityList);
    }
}