
export interface LanguageContent{
    app:{
        title:string;
    }
    component:{
        loading:string;
    }
    action:{
        menue:string;
        add:string;
        close:string;
        save:string;
    }
    addgoal:{
        title:string;
        icon:string;
        color:string;
    },
    message: {
        please_enter_title:string,
        delete:string,
        add_to_home:string,
        new_database:string,
        add_example:string
    }

}

export class Language{
    lang: string;
    
    constructor(lang:string){
        this.lang = lang;
    }

    public getCurrent():LanguageContent{
        let language = this.getLanguageList().get(this.lang);
        if(language == null){
            return this.getLanguageList().get("en");
        }

        return language;
    }

    public getLanguageList():Map<string, LanguageContent>{
       return new Map([
           ["en",{
               app: {
                        title:"Life Goals"
                    },
                    component:{
                        loading: "Loading..."
                    },
                    action: {
                        menue: "More",
                        add:"add",
                        close: "close",
                        save: "Save"
                    },
                    addgoal:{
                        title:"Title: ",
                        icon: "Icon: ",
                        color: "Color: "
                    },
                    message:{
                        please_enter_title:"Please enter title",
                        delete: "Delete?",
                        add_to_home: "Click on the share menue and then 'Add to Homescreen'",
                        new_database:"New Version available. All your data will be deleted: \n\n",
                        add_example:"No data found. Add example data?"
                    }
                }],
                ["de", {
                    app: {
                        title:"Life Goals"
                    },
                    component:{
                        loading: "Laden..."
                    },
                    action: {
                        menue: "Screen",
                        add:"Hinzu",
                        close: "Zurück",
                        save:"Speichern"
                    },
                    addgoal:{
                        title:"Titel: ",
                        icon: "Symbol: ",
                        color: "Farbe: "
                    },
                   message:{
                       please_enter_title:"Bitte einen Titel eingeben",
                       delete: "Löschen?",
                       add_to_home:"Klicke auf das Teilen-Menü und dann auf 'zum Home-Bildschirm'",
                       new_database:"Es gibt eine neue Version. Alle deine Daten werden gelöscht: \n\n",
                       add_example:"Es wurden keine Einträge gefunden. Sollen Beispieldaten hinzugefügt werden?"
                   }
                }]
        ]);
    }
}

export const lang: LanguageContent = new Language("de").getCurrent();