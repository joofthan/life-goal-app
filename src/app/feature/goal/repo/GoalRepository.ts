import type { Goal } from "../entity/Goal";
import { BaseRepository } from "../../../core/infrastructure/BaseRepository";
import { lang } from "../infrastructure/language";

export class GoalRepository extends BaseRepository<Goal>{

    heart = {
        id:"heart",
        color:"red"
    };
    book = {
        id:"book",
        color:"blue"
    };
    routine = {
        id:"redo",
        color:"rgb(170, 170, 170)"
    }

    example:Goal[] = [
        {
            id:1,
            icon: this.routine,
            title:"Routinen",
            subgoals:[
                {title:"6 Uhr aufstehen", isChecked:false},
                {title:"Meditieren", isChecked:false},
                {title:"Trainieren", isChecked:false},
                {title:"Tagebuch schreiben", isChecked:false}
            ]
        }
        ,{
            id:2,
            icon: this.book,
            title:"Weiterbildung",
            subgoals:[
                {title:"Buch1 lesen", isChecked:true},
                {title:"Buch2 lesen", isChecked:false}
            ]
        }
        ,
        {
            id:3,
            icon: this.heart,
            title:"Partner finden",
            subgoals:[
                {title:"Neue Leute kennenlernen", isChecked:true},
                {title:"Nach Date fragen", isChecked:true},
                {title:"Zusammenziehen", isChecked:false},
                {title:"Kinder großziehen", isChecked:false}
            ]
        }
        ,
        {
            id:4,
            icon: {
                id:"money-bill-alt",
                color:"green"
            },
            title:"Passives Einkommen",
            subgoals:[
                {
                    title:"10.000 € investieren",
                    isChecked: false
                }
            ]
        },
        {
            id:5,
            icon: {
                id:"building",
                color:"rgb(130, 100, 50)"
            },
            title:"Business aufbauen",
            subgoals:[
                {title:"Businessplan erstellen",isChecked:false},
                {title:"Investoren suchen",isChecked:false}
            ]
        }

    ];
    asked=false;

    constructor(){
        super('goals');
    }

    public async findAll():Promise<Goal[]>{
        let all = await super.findAll();
        if(all.length == 0){
            if(!this.asked && confirm(lang.message.add_example)){
                await this.db.write(this.key, this.example);
            }
            this.asked = true;
            
            return super.findAll();
        }
        return all;
    }
}