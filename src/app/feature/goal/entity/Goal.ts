import { BaseEntity } from "src/app/core/infrastructure/BaseEntity";

class Icon{
    id:string;
    color:string;
}

export class Goal extends BaseEntity{
    title:string;
    icon:Icon;
    subgoals: SubGoal[];
}

export class SubGoal{
    title: string;
    isChecked:boolean;
}