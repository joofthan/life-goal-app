import type { UseCaseInteractor } from "../../../core/interactor/UseCaseInteractor";
import type { GoalRepository } from "../repo/GoalRepository";

class RemoveGoalRequest{
    id:number;
}

class RemoveGoalResponse{

}

export class RemoveGoal implements UseCaseInteractor<RemoveGoalRequest,RemoveGoalResponse>{
    goalRepository: GoalRepository;
    constructor(goalRepository:GoalRepository){
        this.goalRepository = goalRepository;
    }

    async execute(req: RemoveGoalRequest): Promise<RemoveGoalResponse> {
        this.goalRepository.delete(req.id);

        return new RemoveGoalResponse();
    }

}