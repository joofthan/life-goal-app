import type { UseCaseInteractor } from "../../../core/interactor/UseCaseInteractor";
import type { GoalRepository } from "../repo/GoalRepository";

class AddSubGoalRequest{
    goalID:number;
    title:string;
}

class AddSubGoalResponse{

}

export class AddSubGoal implements UseCaseInteractor<AddSubGoalRequest,AddSubGoalResponse>{
    goalRepository: GoalRepository;
    constructor(goalRepository:GoalRepository){
        this.goalRepository = goalRepository;
    }

    async execute(req: AddSubGoalRequest): Promise<AddSubGoalResponse> {
        let goal = await this.goalRepository.findById(req.goalID);
        goal.subgoals.push({
            title: req.title,
            isChecked:false
        });
        this.goalRepository.save(goal);
        return new AddSubGoalResponse();
    }

}