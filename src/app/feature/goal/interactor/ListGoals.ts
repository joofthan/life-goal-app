import type { UseCaseInteractor } from "src/app/core/interactor/UseCaseInteractor";
import type { Goal } from "../entity/Goal";
import type { GoalRepository } from "../repo/GoalRepository";

class ListGoalsResponse{
    public goals:Goal[];
}

export class ListGoals implements UseCaseInteractor<void,ListGoalsResponse>{
    goalRepository: GoalRepository;
    constructor(goalRepository:GoalRepository){
        this.goalRepository = goalRepository;
    }

    async execute(req: void): Promise<ListGoalsResponse> {
        let list = await this.goalRepository.findAll();

        return {goals:list}
    }

}