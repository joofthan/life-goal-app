import type { UseCaseInteractor } from "src/app/core/interactor/UseCaseInteractor";
import type { Goal } from "../entity/Goal";
import type { GoalRepository } from "../repo/GoalRepository";

class LoadDetailsRequest{
    public id:number;
}

class LoadDetailsResponse{
    public goal:Goal;
}

export class LoadDetails implements UseCaseInteractor<LoadDetailsRequest,LoadDetailsResponse>{
    goalRepository: GoalRepository;
    constructor(goalRepository:GoalRepository){
        this.goalRepository = goalRepository;
    }

    async execute(req: LoadDetailsRequest): Promise<LoadDetailsResponse> {
        let goal = await this.goalRepository.findById(req.id);
        return {goal:goal}
    }

}