import type { UseCaseInteractor } from "../../../core/interactor/UseCaseInteractor";
import type { Goal } from "../entity/Goal";
import type { GoalRepository } from "../repo/GoalRepository";

class SaveGoalRequest{
    goal:Goal;
}

class SaveGoalResponse{

}

export class SaveGoal implements UseCaseInteractor<SaveGoalRequest,SaveGoalResponse>{
    goalRepository: GoalRepository;
    constructor(goalRepository:GoalRepository){
        this.goalRepository = goalRepository;
    }

    async execute(req: SaveGoalRequest): Promise<SaveGoalResponse> {
        for (let index = 0; index < req.goal.subgoals.length; index++) {
            const subgoal = req.goal.subgoals[index];
            if(subgoal.title === ""){
                req.goal.subgoals.splice(index, 1);
            }
            
        }
        this.goalRepository.save(req.goal);
        return new SaveGoalResponse();
    }

}