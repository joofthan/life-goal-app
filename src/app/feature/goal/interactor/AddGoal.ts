import type { UseCaseInteractor } from "../../../core/interactor/UseCaseInteractor";
import type { GoalRepository } from "../repo/GoalRepository";

class AddGoalRequest{
    title:string;
    iconId:string;
    color:string;
}

class AddGoalResponse{

}

export class AddGoal implements UseCaseInteractor<AddGoalRequest,AddGoalResponse>{
    goalRepository: GoalRepository;
    constructor(goalRepository:GoalRepository){
        this.goalRepository = goalRepository;
    }

    async execute(req: AddGoalRequest): Promise<AddGoalResponse> {
        this.goalRepository.save({
            id: null,
            icon: {id:req.iconId, color:req.color},
            title: req.title,
            subgoals: []
        });
        return new AddGoalResponse();
    }

}