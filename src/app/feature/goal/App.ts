import { navigator , AppNavigator} from "../../core/infrastructure/AppNavigator";
import { AddGoal } from "./interactor/AddGoal"
import { AddSubGoal } from "./interactor/AddSubgoal";
import { ListGoals } from "./interactor/ListGoals";
import { LoadDetails } from "./interactor/LoadDetails";
import { RemoveGoal } from "./interactor/RemoveGoal";
import { SaveGoal } from "./interactor/SaveGoal";
import { GoalRepository } from "./repo/GoalRepository";

export class App{
    constructor(
        public navigate: AppNavigator,
        public addGoal:AddGoal, 
        public removeGoal:RemoveGoal, 
        public listGoals:ListGoals, 
        public loadDetails: LoadDetails,
        public addSubGoal: AddSubGoal,
        public saveGoal: SaveGoal
     ){}


    static async createDefault():Promise<App>{
        let goalRepository = new GoalRepository();
        return new App(
            navigator,
            new AddGoal(goalRepository),
            new RemoveGoal(goalRepository),
            new ListGoals(goalRepository),
            new LoadDetails(goalRepository),
            new AddSubGoal(goalRepository),
            new SaveGoal(goalRepository)
            );
    }
}